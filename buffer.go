package encoding

import (
	"encoding/binary"
	"errors"
	"io"
)

// Buffer is a variable-sized buffer of bytes used to encode and decode
// primitives and messages.
type Buffer struct {
	order binary.ByteOrder

	data []byte
	err  error
}

const (
	bootstrapSize = 64 // helps small buffers avoid allocation

	maxString = 1<<31 - 1
)

type BufferOption func(*Buffer)

func ByteOrder(order binary.ByteOrder) BufferOption {
	return func(b *Buffer) {
		if order != nil {
			b.order = order
		}
	}
}

// NewBuffer creates and initializes a new Buffer using data as its
// initial contents. The new Buffer takes ownership of data, and the
// caller should not use data after this call.
func NewBuffer(data []byte, opts ...BufferOption) *Buffer {
	b := &Buffer{data: data}
	for _, fn := range opts {
		fn(b)
	}
	return b
}

// Reset resets the buffer to be empty, but it retains the underlying
// storage for use by future.
func (b *Buffer) Reset() {
	b.err = nil
	b.data = b.data[:0]
}

// Bytes returns a slice of length b.Len() holding the unread portion of
// the buffer. The slice is valid for use only until the next buffer
// modification.
func (b Buffer) Bytes() []byte { return b.data }

// Len returns the number of bytes of the unread portion of the buffer.
func (b Buffer) Len() int { return len(b.data) }

// Err returns the first error that was encountered by the Buffer.
func (b Buffer) Err() error { return b.err }

func (b Buffer) byteOrder() binary.ByteOrder {
	if b.order != nil {
		return b.order
	}
	return binary.LittleEndian
}

const maxInt = int(^uint(0) >> 1)

func (b *Buffer) grow(n int) (off int) {
	if b.data == nil {
		if n < bootstrapSize {
			b.data = make([]byte, n, bootstrapSize)
		} else {
			b.data = make([]byte, n)
		}
		return 0
	}

	off = len(b.data)
	if off+n > cap(b.data) {
		capacity := n + cap(b.data)*2
		if capacity >= maxInt {
			panic("buffer too large")
		}
		if capacity > 4*1024*1024 {
			capacity = n + cap(b.data)
		}

		data := make([]byte, off, capacity)
		copy(data[:off], b.data)
		b.data = data
	}

	b.data = b.data[:off+n]
	return off
}

// Write appends the contents of p to the buffer, growing the buffer as
// needed. The return value is the length of p. Err is always nil.
func (b *Buffer) Write(p []byte) (int, error) {
	off := b.grow(len(p))
	return copy(b.data[off:], p), nil
}

// WriteString appends the contents of s to the buffer, growing the
// buffer as needed. The return value is the length of p. Err is always
// nil.
func (b *Buffer) WriteString(s string) (int, error) {
	off := b.grow(len(s))
	return copy(b.data[off:], s), nil
}

// PutStringV encodes a varint count-delimited string into Buffer.
func (b *Buffer) PutStringV(v string) {
	n := b.grow(binary.MaxVarintLen64 + len(v))
	n += binary.PutUvarint(b.data[n:], uint64(len(v)))
	n += copy(b.data[n:], v)
	b.data = b.data[:n]
}

// PutString32 encodes a unsigned 32-bit count-delimited string into
// Buffer.
func (b *Buffer) PutString32(v string) {
	n := b.grow(4 + len(v))
	b.byteOrder().PutUint32(b.data[n:], uint32(len(v)))
	copy(b.data[n+4:], v)
}

// PutString16 encodes a unsigned 16-bit count-delimited string into
// Buffer.
func (b *Buffer) PutString16(v string) {
	n := b.grow(2 + len(v))
	b.byteOrder().PutUint16(b.data[n:], uint16(len(v)))
	copy(b.data[n+2:], v)
}

// PutString8 encodes a unsigned 8-bit count-delimited string into
// Buffer.
func (b *Buffer) PutString8(v string) {
	n := b.grow(1 + len(v))
	b.data[n] = uint8(len(v))
	copy(b.data[n+1:], v)
}

// PutVarint varint encodes a unsigned 64-bit integer into Buffer.
func (b *Buffer) PutVarint(v uint64) {
	n := b.grow(binary.MaxVarintLen64)
	n += binary.PutUvarint(b.data[n:], v)
	b.data = b.data[:n]
}

// PutUint64 encodes a unsigned 64-bit integer into Buffer.
func (b *Buffer) PutUint64(v uint64) {
	n := b.grow(8)
	b.byteOrder().PutUint64(b.data[n:], v)
}

// PutUint32 encodes a unsigned 32-bit integer into Buffer.
func (b *Buffer) PutUint32(v uint32) {
	n := b.grow(4)
	b.byteOrder().PutUint32(b.data[n:], v)
}

// PutUint16 encodes a unsigned 16-bit integer into Buffer.
func (b *Buffer) PutUint16(v uint16) {
	n := b.grow(2)
	b.byteOrder().PutUint16(b.data[n:], v)
}

// PutUint8 encodes a unsigned 8-bit integer into Buffer.
func (b *Buffer) PutUint8(v uint8) {
	n := b.grow(1)
	b.data[n] = v
}

// consume consumes n bytes from Buffer. If an error occurred it returns
// false.
func (b *Buffer) consume(n int) ([]byte, bool) {
	if b.err != nil {
		return nil, false
	}
	if len(b.data) < n {
		b.err = io.ErrUnexpectedEOF
		return nil, false
	}
	data := b.data[:n]
	b.data = b.data[n:]
	return data, true
}

// varint decoded the next varint from Buffer. If an error occurred it
// returns false.
func (b *Buffer) varint() (uint64, bool) {
	if b.err != nil {
		return 0, false
	}

	v, n := binary.Uvarint(b.data)
	if n == 0 {
		b.err = io.ErrUnexpectedEOF
		return 0, false
	}
	if n < 0 {
		b.err = errors.New("64-bit overflow")
		return 0, false
	}

	b.data = b.data[n:]
	return v, true
}

// string decodes the next string from Buffer.
func (b *Buffer) string(size uint64) string {
	if size > maxString {
		b.err = errors.New("string too large")
		return ""
	}
	if uint64(len(b.data)) < size {
		b.err = io.ErrUnexpectedEOF
		return ""
	}

	v := string(b.data[:size])
	b.data = b.data[size:]
	return v
}

// StringV decodes a varint count-delimited string from Buffer. If an
// error occurred, the value is an empty string.
func (b *Buffer) StringV() string {
	size, ok := b.varint()
	if !ok {
		return ""
	}
	return b.string(size)
}

// Varint varint decodes a unsigned 64-bit integer from Buffer. If an
// error occurred, the value is 0.
func (b *Buffer) Varint() uint64 {
	v, _ := b.varint()
	return v
}

// String32 decodes a unsigned 32-bit count-delimited string from
// Buffer. If an error occurred, the value is an empty string.
func (b *Buffer) String32() string {
	size := uint64(b.Uint32())
	return b.string(size)
}

// String16 decodes a unsigned 16-bit count-delimited string from
// Buffer. If an error occurred, the value is an empty string.
func (b *Buffer) String16() string {
	size := uint64(b.Uint16())
	return b.string(size)
}

// String8 decodes a unsigned 8-bit count-delimited string from Buffer.
// If an error occurred, the value is an empty string.
func (b *Buffer) String8() string {
	size := uint64(b.Uint8())
	return b.string(size)
}

// Uint64 decodes a unsigned 64-bit integer from Buffer. If an error
// occurred, the value is 0.
func (b *Buffer) Uint64() uint64 {
	data, ok := b.consume(8)
	if !ok {
		return 0
	}
	return b.byteOrder().Uint64(data)
}

// Uint32 decodes a unsigned 32-bit integer from Buffer. If an error
// occurred, the value is 0.
func (b *Buffer) Uint32() uint32 {
	data, ok := b.consume(4)
	if !ok {
		return 0
	}
	return b.byteOrder().Uint32(data)
}

// Uint16 decodes a unsigned 16-bit integer from Buffer. If an error
// occurred, the value is 0.
func (b *Buffer) Uint16() uint16 {
	data, ok := b.consume(2)
	if !ok {
		return 0
	}
	return b.byteOrder().Uint16(data)
}

// Uint8 decodes a unsigned 0-bit integer from Buffer. If an error
// occurred, the value is 0.
func (b *Buffer) Uint8() uint8 {
	data, ok := b.consume(1)
	if !ok {
		return 0
	}
	return data[0]
}
