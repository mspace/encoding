package encoding

import "testing"

const N = 10000 // make this bigger for a larger (and slower) test; data for write tests

var (
	testString string
	testBytes  []byte
)

func init() {
	testBytes = make([]byte, N)
	for i := 0; i < N; i++ {
		testBytes[i] = 'a' + byte(i%26)
	}
	testString = string(testBytes)
}

func checkData(t *testing.T, name string, buf *Buffer, want string) {
	t.Helper()
	bytes := buf.Bytes()

	if buf.Len() != len(bytes) {
		t.Errorf("%s: expected len %d, have %d", name, buf.Len(), len(want))
	}

	if buf.Len() != len(want) {
		t.Errorf("%s: expected len %d, have %d", name, buf.Len(), len(want))
	}

	if string(bytes) != want {
		t.Errorf("%s: expected content %q, have %q", name, bytes, want)
	}
}

func TestNewBuffer(t *testing.T) {
	buf := NewBuffer(testBytes)
	checkData(t, "NewBuffer", buf, testString)
}

func TestWrite(t *testing.T) {
	buf := Buffer{}

	for i := 0; i < 5; i++ {
		checkData(t, "Write", &buf, "")

		buf.Reset()
		checkData(t, "Write", &buf, "")

		buf.Write(testBytes[0:26])
		checkData(t, "Write", &buf, testString[0:26])

		buf.Reset()
		checkData(t, "Write", &buf, "")

		buf.WriteString(testString[0:26])
		checkData(t, "Write", &buf, testString[0:26])
		buf.Reset()
	}
}

func checkFixed(t *testing.T, name string, buf *Buffer, v interface{}) {
	t.Helper()

	switch want := v.(type) {
	case uint64:
		if buf.Uint64() != want {
			t.Errorf("%s: expected value %d, have %d", name, want, buf.Uint64())
		}
	case uint32:
		if buf.Uint32() != want {
			t.Errorf("%s: expected value %d, have %d", name, want, buf.Uint32())
		}
	case uint16:
		if buf.Uint16() != want {
			t.Errorf("%s: expected value %d, have %d", name, want, buf.Uint16())
		}
	case uint8:
		if buf.Uint8() != want {
			t.Errorf("%s: expected value %d, have %d", name, want, buf.Uint8())
		}
	}
}

func checkString(t *testing.T, name string, buf *Buffer, size int, want string) {
	t.Helper()

	var got string
	switch size {
	case 32:
		got = buf.String32()
	case 16:
		got = buf.String16()
	case 8:
		got = buf.String8()
	}
	if got != want {
		t.Errorf("%s: expected %q, have %q", name, want, got)
	}
}

func TestFixedEncoding(t *testing.T) {
	buf := Buffer{}

	checkFixed(t, "FixedEncoding", &buf, uint8(0))
	checkFixed(t, "FixedEncoding", &buf, uint16(0))
	checkFixed(t, "FixedEncoding", &buf, uint32(0))
	checkFixed(t, "FixedEncoding", &buf, uint64(0))

	buf.Reset()

	buf.PutUint8(8)
	buf.PutUint16(16)
	buf.PutUint32(32)
	buf.PutUint64(64)

	checkFixed(t, "FixedEncoding", &buf, uint8(8))
	checkFixed(t, "FixedEncoding", &buf, uint16(16))
	checkFixed(t, "FixedEncoding", &buf, uint32(32))
	checkFixed(t, "FixedEncoding", &buf, uint64(64))
}

func TestStringEncoding(t *testing.T) {
	buf := Buffer{}

	checkString(t, "StringEncoding", &buf, 8, "")
	checkString(t, "StringEncoding", &buf, 16, "")
	checkString(t, "StringEncoding", &buf, 32, "")

	buf.Reset()

	buf.PutString8("howdie 8")
	buf.PutString16("howdie 16")
	buf.PutString32("howdie 32")

	checkString(t, "StringEncoding", &buf, 8, "howdie 8")
	checkString(t, "StringEncoding", &buf, 16, "howdie 16")
	checkString(t, "StringEncoding", &buf, 32, "howdie 32")
}
